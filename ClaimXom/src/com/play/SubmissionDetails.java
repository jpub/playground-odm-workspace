package com.play;

public class SubmissionDetails {
	private String parentId;
	private String childId;
	private int numberOfDays;
	private int salaryPerDay;
	
	public SubmissionDetails() {
		super();
		this.parentId = "";
		this.childId = "";
		this.numberOfDays = 0;
		this.salaryPerDay = 0;
	}
	
	public SubmissionDetails(String parentId, String childId, int numberOfDays, int salaryPerDay) {
		super();
		this.parentId = parentId;
		this.childId = childId;
		this.numberOfDays = numberOfDays;
		this.salaryPerDay = salaryPerDay;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public int getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public int getSalaryPerDay() {
		return salaryPerDay;
	}

	public void setSalaryPerDay(int salaryPerDay) {
		this.salaryPerDay = salaryPerDay;
	}	
}
