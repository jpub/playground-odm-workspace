package com.play;

public class ValidationDetails {
	private int maxClaimAmount;
	private String parentId;
	private String childId;
	private String icaChildId;
	private String fsrChildId;
	private String fsrSpouseId;
	
	public ValidationDetails() {
		super();
		this.maxClaimAmount = 0;
		this.parentId = "";
		this.childId = "";
		this.icaChildId = "";
		this.fsrChildId = "";
		this.fsrSpouseId = "";
	}
	
	public ValidationDetails(int maxClaimAmount, String parentId, String childId, String icaChildId, String fsrChildId, String fsrSpouseId) {
		super();
		this.maxClaimAmount = maxClaimAmount;
		this.parentId = parentId;
		this.childId = childId;
		this.icaChildId = icaChildId;
		this.fsrChildId = fsrChildId;
		this.fsrSpouseId = fsrSpouseId;
	}

	public int getMaxClaimAmount() {
		return maxClaimAmount;
	}

	public void setMaxClaimAmount(int maxClaimAmount) {
		this.maxClaimAmount = maxClaimAmount;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public String getIcaChildId() {
		return icaChildId;
	}

	public void setIcaChildId(String icaChildId) {
		this.icaChildId = icaChildId;
	}
	
	public String getFsrChildId() {
		return fsrChildId;
	}

	public void setFsrChildId(String fsrChildId) {
		this.fsrChildId = fsrChildId;
	}

	public String getFsrSpouseId() {
		return fsrSpouseId;
	}

	public void setFsrSpouseId(String fsrSpouseId) {
		this.fsrSpouseId = fsrSpouseId;
	}
}
