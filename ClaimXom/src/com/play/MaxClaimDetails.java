package com.play;

public class MaxClaimDetails {
	private int maxClaimAmount;
	private String parentId;
	private String childId;
	
	public MaxClaimDetails() {
		super();
		this.maxClaimAmount = 0;
		this.parentId = "";
		this.childId = "";
	}
	
	public MaxClaimDetails(int maxClaimAmount, String parentId, String childId) {
		super();
		this.maxClaimAmount = maxClaimAmount;
		this.parentId = parentId;
		this.childId = childId;
	}

	public int getMaxClaimAmount() {
		return maxClaimAmount;
	}

	public void setMaxClaimAmount(int maxClaimAmount) {
		this.maxClaimAmount = maxClaimAmount;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}			
}
